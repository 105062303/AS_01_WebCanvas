var cPushArray = new Array();
var cStep = -1;
var ctx;
 ctx = document.getElementById('canvas').getContext("2d");
var undo = document.getElementById('undo');
var redo = document.getElementById('redo');
     
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
}          

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}                

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}                       

undo.addEventListener('click', function(){
    cUndo();
});
redo.addEventListener('click', function(){
    cRedo();
});