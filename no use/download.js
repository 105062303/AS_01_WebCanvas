function download(){ 
    var canvas=document.querySelector('#canvas'); 
    var ctx=canvas.getContext("2d"); 
    var base64Img = canvas.toDataURL(); 
    var oA = document.createElement('a'); 
    oA.href = base64Img; 
    oA.download = 'your picture.png'; 
  
    var event = document.createEvent('MouseEvents'); 
    event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); 
    oA.dispatchEvent(event); 
  };