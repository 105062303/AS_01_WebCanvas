(function() {
    var d = document,
        canvasDom = d.getElementById('canvas'),
        imageDom = d.getElementById('eraser'),
        ctx = canvasDom.getContext('2d'),
        isErasing = false,
        size = 10; // Radius of eraser
    
    ctx.fillStyle = 'rgb(0, 255, 0)';
    ctx.beginPath();
    ctx.rect(0, 0, 400, 400);
    ctx.fill();

    ctx.globalCompositeOperation = "copy";
    ctx.strokeStyle = "rgba(0,0,0,0)"; // Drawing with a transparent color simulates eraser
    
    function draw(x, y) {
        ctx.beginPath();
        ctx.arc(x, y, size, 0, 2 * Math.PI, false);
        ctx.stroke();
    }

    canvasDom.addEventListener('mousemove', function(e) {
        if (!isErasing) {
            return false;
        }

        draw(e.clientX, e.clientY);
    }, false);

    canvasDom.addEventListener('mousedown', function(e) {
        isErasing = true;
    }, false);

    canvasDom.addEventListener('mouseup', function(e) {
        isErasing = false;
    }, false);
})();