function Init() {
 
    var canvas = document.getElementById('canvas');
    var canvasTmp = document.getElementById('canvasTmp');
    
    //test1();

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvasTmp.width = canvas.width;
    canvasTmp.height = canvas.height;
  
    //var ctx = canvas.getContext('2d');
    //var context = canvas.getContext('2d');
    var ctxTmp = canvasTmp.getContext('2d');
  
    //w = canvas.width;
    //h = canvas.height;
    
    canvas.addEventListener('mousedown', function (e) { handleToolDown(e) }, false);
    canvas.addEventListener('mouseup', function (e) { handleToolUp(e) }, false);
    canvas.addEventListener('mousemove', function (e) { handleToolMove(e) }, true);
  
    canvasTmp.addEventListener('mousedown', function (e) { handleShapeDown(e) }, false);
    canvasTmp.addEventListener('mouseup', function (e) { handleShapeUp(e)}, false);
    canvasTmp.addEventListener('mousemove', function (e) { handleShapeMove(e) }, false);
    
    
    //Tmp layers
    canvasTmp.style.display = 'none';
    
  
}

var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');


//cursor
//$('canvas').css('cursor', 'url(th.jpg), auto');
var radius = 10;
var drawing = false;
var shape = false;
var cantext = false;
var tri = false;
var rec = false;
var cir = false;
var eraser = false;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

context.lineWidth = radius*2;

var putPoint = function(e){
    if(drawing){
        context.stroke();
        context.lineWidth = radius*2;
        context.lineTo(e.clientX, e.clientY);
        context.stroke();
        context.beginPath();
        context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(e.clientX, e.clientY);
    }
}

var engage = function(e){
    if(cantext||tri||rec||cir||eraser){
        drawing = false;
    }
    else{
        cantext = false;
        tri = false;
        rec = false;
        cir = false;
        eraser = false;
        drawing = true;
        putPoint(e);
    }
}

var disengage = function(){
    drawing = false;
    context.beginPath();
    //cPush();
    var state = context.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
}

canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mouseup', disengage);

//undoredo
let state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  // 清除畫布
  context.clearRect(0, 0, canvas.width, canvas.height);

  // 透過 e.state 取得先前存到 window.history 的狀態
  if( e.state ){
    context.putImageData(e.state, 0, 0);
  }
}

//undoredo

//shape-----------------------------------------------------------------------
function test4(){
    tri=true;
    //test1();
    triangle();

}

//var canvas = document.getElementById('canvas');
//var context = canvas.getContext('2d');
 
// the triangle
function triangle(){
    
    if(tri){
    context.beginPath();
    context.moveTo(300, 300);
    context.lineTo(300, 500);
    context.lineTo(500, 500);
    context.closePath();
 
// the outline
    context.lineWidth = 40;
    context.strokeStyle = 'black';
    context.stroke();
 
// the fill color
    //context.fillStyle = "#FFCC00";
    //context.fill();
    tri = false;
    //test1();
    }
}

function test5(){
    rec = true;
    rectangle();
}

function rectangle(){
    if(rec){
    context.beginPath();
    context.lineWidth="40";
    context.strokeStyle='black';
    context.rect(920,320,300,180);
    context.stroke();
    rec = false;
}
}

function test6(){
    cir = true;
    circle();
}

function circle(){
    var centerX = canvas.width / 2;
    var centerY = canvas.height / 2;
    var cirradius = 130;

    context.beginPath();
    context.arc(centerX, centerY, cirradius, 0, 2 * Math.PI, false);
    context.fillStyle = 'black';
    //context.fill();
    context.lineWidth = 40;
    context.strokeStyle = 'black';
    context.stroke();
    cir = false;
}

//shape-------------------------------------------------------------------

/*
var cPushArray = new Array();
var cStep = -1;
//var ctx;
//ctx = document.getElementById('canvas').getContext("2d");
var undo = document.getElementById('undo');
var redo = document.getElementById('redo');
     
function cPush(){
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
}          

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    }
}                

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    }
}                       

undo.addEventListener('click', function(){
    cUndo();
});
redo.addEventListener('click', function(){
    cRedo();
});
*/

//clear
var clearButton = document.getElementById('clearbutton');
function clear(){
    context.clearRect( 0, 0, window.innerWidth, window.innerHeight);
    cPushArray.clear();
    cPushArray.length = 0;
    cStep = -1;
}
clearButton.addEventListener('click', clear);
//clear

//download
function download(){ 
    var canvas=document.querySelector('#canvas'); 
    var ctx=canvas.getContext("2d"); 
    var base64Img = canvas.toDataURL(); 
    var oA = document.createElement('a'); 
    oA.href = base64Img; 
    oA.download = 'your picture.png'; 
  
    var event = document.createEvent('MouseEvents'); 
    event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); 
    oA.dispatchEvent(event); 
  };
//download


 
    var font = '14px sans-serif',
    hasInput = false;

    function test2()
    {
        hasInput = false;
        cantext = true;
        test3();
    }

    function test3(){
        if(cantext){
            canvas.onclick = function(e) {
                if (hasInput) return;
                addInput(e.clientX, e.clientY);
            }
        }
    }



function addInput(x, y) {
    
    var input = document.createElement('input');
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;
    
    document.body.appendChild(input);

    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode == 13) {
        //test1();
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        //hasInput = false;
        cantext = false;
    }
}

function drawText(txt, x, y) {
    
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = font;
    context.fillText(txt, x - 4, y - 4);
    cantext = false;
}

//upload
var imageLoader = document.getElementById('imageLoader'); 
    imageLoader.addEventListener('change', handleImage, false); 
var canvas = document.getElementById('canvas'); 
//var ctx = canvas.getContext('2d'); 

 
function handleImage(e){ 
    context.globalAlpha = 1;
    var reader = new FileReader(); 
    reader.onload = function(event){ 
        var img = new Image(); 
        img.onload = function(){ 
            //canvas.width = img.width; 
            //canvas.height = img.height; 
            context.drawImage(img,10,160); 
        } 
        img.src = event.target.result; 
    } 
    reader.readAsDataURL(e.target.files[0]);      
}

//upload



      function writeMessage(canvas, message) {
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, 280, 160);
        context.font = '18pt Calibri';
        //context.fillStyle = 'black';
        context.fillText(message, 10, 120);
      }
      function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
      }

      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');

      canvas.addEventListener('mousemove', function(evt) {
        var mousePos = getMousePos(canvas, evt);
        var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
        writeMessage(canvas, message);
      }, false);


//---------------------------------------------------------------------------------------
/*      function test1() 
{  
    document.getElementById("fileUpload").addEventListener("change",readImage,false);
  fileUpload.click();   
  textfield.value = upload.value;   
}

function readImage(){
    canvas = document.getElementById('canvas');
  
    canvas.width = 1065;
    canvas.height = 600;
    canvasGrid.width = canvas.width;
    canvasGrid.height = canvas.height;
    canvasTmp.width = canvas.width;
    canvasTmp.height = canvas.height;
  
    ctx = canvas.getContext('2d');
    ctxGrid = canvasGrid.getContext('2d');
    ctxTmp = canvasTmp.getContext('2d');
    w = canvas.width;
    h = canvas.height;
    if(this.files&&this.files[0]){
      ctx.clearRect(0, 0,w, h);
      clearpad();
      var FR = new FileReader();
      FR.onload = function(event) {
        var img = new Image();
        img.addEventListener("load", function() {
          ctx.drawImage(img, 0, 0,w, h);
          cPush();
        });
        img.src = event.target.result;
      };       
      FR.readAsDataURL( this.files[0] );
    }
  }
*/
  function test7(){
        //context.lineWidth = radius;
        context.globalAlpha = 0.4;
        context.drawImage(crayonTextureImage, 0, 0, canvas.width, canvas.height);
    
        //context.globalAlpha = 1;
  }

  function test8(){
      context.globalAlpha = 1;
  }
  
